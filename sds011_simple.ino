#include <SDS011.h>

#define PIN_SDS011_TX 4
#define PIN_SDS011_RX 3

#define INTERVAL_SDS011 30

float p10, p25;
int error;

SDS011 sds;
unsigned long timerSDS = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Init");

  sds.begin(PIN_SDS011_RX, PIN_SDS011_TX);
  Serial.println("SDS011 inited");
//  sds.wakeup();
//  delay(10);
//  sds.wakeup();
//  sds.continuous_mode();
  
//  readSDS011();
//  sds.sleep();
}

void loop() {
  loopSDS011();
//  delay(10000);
//  Serial.println(round((float)(millis() - timerSDS)/1000));
}

void loopSDS011()
{
  if (millis() - timerSDS >= INTERVAL_SDS011*1000){

//    sds.wakeup();
//    delay(10000);
    readSDS011();
//    sds.sleep();

    timerSDS = millis();
  }
}

void readSDS011()
{
    error = sds.read(&p25, &p10);
    if (!error) {
      Serial.println("P2.5: " + String(p25));
      Serial.println("P10:  " + String(p10));
    }else{
      Serial.println("Error:  " + String(error));
    }  
}
